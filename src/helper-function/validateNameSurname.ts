type ErrorPeturnType = {
    status: 'fail';
    errorMessage: string;
}

type SuccessPeturnType = {
    status: 'success';
}

type ValidateNameSurnameReturnType = ErrorPeturnType | SuccessPeturnType;

export default function validateNameSurname(name: string, type: "Name" | "Surname"): ValidateNameSurnameReturnType{
    const regexNoWhitespaces = /^\b\S+\b$/g;
    if(!name.match(regexNoWhitespaces)){
        return {
            status: 'fail',
            errorMessage: `${type} must contain no whitespaces`, 
        };
    }

    const regexOnlyLattinLetters = /^\b[A-Z][a-z]*\b$/g;
    const regexOnlyLattinLettersWithHyphen = /^\b[A-Z][a-z]*-[A-Z][a-z]*\b$/g;
    if(!name.match(regexOnlyLattinLetters) && !name.match(regexOnlyLattinLettersWithHyphen)){
        return {
            status: 'fail',
            errorMessage: `${type} must contain only latin letters and '-'.`, 
        };
    }

    return {
        status: "success"
    };
}