import { Student } from "../types/Student";


export function getDummyTasks(): Student[]{
    return [
        {
            id: 1,
            group: "AI-11",
            firstName: "James",
            lastName: "Bond",
            gender: "male",
            birthday: "11.11.1111",
            status: "alive",
        },
        {
            id: 2,
            group: "PZ-27",
            firstName: "Ann",
            lastName: "Bond",
            gender: "female",
            birthday: "11.11.1111",
            status: "dead",
        },
        {
            id: 2,
            group: "PZ-27",
            firstName: "Ann",
            lastName: "Bond",
            gender: "female",
            birthday: "11.11.1111",
            status: "dead",
        }
    ]
}

export function getDefaultStudent(): Student
{
    return {
        id: 0,
        group: "PZ-21",
        firstName: "",
        lastName: "",
        gender: "male",
        birthday: "",
        status: "alive",
    }
}
