export type Student = {
    id: number;
    group: string;
    firstName: string;
    lastName: string;
    gender: "male" | "female";
    birthday: string;
    status: "dead" | "alive";
}

export type StudentCreate = {
    group: string;
    firstName: string;
    lastName: string;
    gender: "male" | "female";
    birthday: string;
    status: "dead" | "alive";
}