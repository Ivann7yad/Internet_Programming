import FRONTEND_ROUTES from "../../common/constants/frontend-routes.constants"
import "./MainHeader.scss"
import HeaderMenu from "./header-menu/HeaderMenu"
import HeaderNotification from "../../common/components/header-components/header-notification/HeaderNotification"
import HeaderUser from "../../common/components/header-components/header-user/HeaderUser"

export default function MainHeader(){
    return (
        <header className="main-header">
            <a href={FRONTEND_ROUTES.PAGES.STUDENTS} className="main-header-text">CMS</a>
            <div className="header-content-holder">
                <div className="notification-user-holder">
                    <HeaderNotification />
                    <HeaderUser />
                </div>
                <HeaderMenu />
            </div>
        </header>
    )   
}