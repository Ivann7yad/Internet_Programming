import './App.scss'
import MainHeader from '../main-header/MainHeader';
import { Outlet } from 'react-router';
import SideBar from '../sidebar/SideBar';
import ITaskAPIService from '../../../api/interfaces/ITaskAPIService';
import IStudentAPIService from '../../../api/interfaces/IStudentAPIService';
import StudentAPIService from '../../../api/StudentAPIService';
import TaskAPIService from '../../../api/TaskAPIService';
import React from 'react';

type APIServices = {
  taskAPIService: ITaskAPIService;
  studentAPIService: IStudentAPIService;
}

export const APIServiceContext = React.createContext<APIServices>({
  taskAPIService: new TaskAPIService(""),
  studentAPIService: new StudentAPIService("")
});

function App() {
  const apiServices: APIServices = {
    taskAPIService: new TaskAPIService(import.meta.env.VITE_API_URL),
    studentAPIService: new StudentAPIService(import.meta.env.VITE_API_URL),
  }

  return (
    <APIServiceContext.Provider value={apiServices}>
      <div>
        <MainHeader />
        <div className="sidebar-main-holder">
          <SideBar />
          <Outlet />
        </div>
      </div>
    </APIServiceContext.Provider>
  )
}

export default App;