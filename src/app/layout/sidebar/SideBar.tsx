import "./SideBar.scss"
import PageLinks from "../../common/components/page-links/PageLinks";

export default function SideBar(){
    return (
        <aside className="sidebar">
            <PageLinks />
        </aside>
    )   
}