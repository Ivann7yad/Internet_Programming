import { Button } from "antd"
import "./OptionsButtons.scss"
import DeleteConfirmationModal from "../../../../app/common/components/modals/delete-confirmation-modal/DeleteConfirmationModal";
import { Student } from "../../../../types/Student";
import CreateUpdateStudentModal from "../../../../app/common/components/modals/create-update-student-modal/CreateUpdateStudentModal";
import { useState } from "react";

type OptionsButtonsProps = {
    student: Student;
    index: number;
}

export default function OptionsButtons({ student }: OptionsButtonsProps){
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState<boolean>(false);
    const [isUpdateModalOpen, setIsUpdateModalOpen] = useState<boolean>(false);

    
    return (
        <div className="options-buttons-holder">
            <DeleteConfirmationModal 
                isModalOpen={isDeleteModalOpen} 
                setIsModalOpen={setIsDeleteModalOpen} 
                id={student.id}
                studentName={student.firstName + " " + student.lastName}/>
            <CreateUpdateStudentModal 
                type="update"
                isModalOpen={isUpdateModalOpen}
                setIsModalOpen={setIsUpdateModalOpen}
                prevStudentData={student}/>
            <Button className="options-button" onClick={() => setIsUpdateModalOpen(true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="black" className="bi bi-pencil" viewBox="0 0 16 16">
                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325"/>
                </svg>
            </Button>
            <Button className="options-button" onClick={() => setIsDeleteModalOpen(true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="black" className="bi bi-x-lg" viewBox="0 0 16 16">
                    <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8z"/>
                </svg>
            </Button>
        </div>
    )   
}